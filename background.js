// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

function isFalse(value) {
    if (value === undefined || !value) {
        return false;
    }

    if ((value.constructor === Array && value.length === 0) || String(value).trim().length === 0 || String(value).trim() === 'false') {
        return true;
    }

    for (var attr in value) {
        return false;
    }

    return true;
}

chrome.runtime.onInstalled.addListener(function () {
    chrome.storage.sync.get(SMART_MUTE_ON_AUDIBLE_CHANGE, function (result) {
        if (isFalse(result)) {
            setStorage(SMART_MUTE_ON_AUDIBLE_CHANGE, true);
        }
    });
    chrome.storage.sync.get(SMART_MUTE_ON_TAB_CHANGE, function (result) {
        if (isFalse(result)) {
            setStorage(SMART_MUTE_ON_TAB_CHANGE, true);
        }
    });
});

chrome.commands.onCommand.addListener((command) => {
    switch (command) {
        case 'mute-others':
            mute('others');
            break;
        case 'unmute-others':
            unmute('others');
            break;
        default:
            break;
    }
});

function handleActiveTabChange() {
    chrome.tabs.query({
        active: true,
        currentWindow: true,
        audible: true
    }, function (currentTabs) {
        if (currentTabs.length > 0) {
            chrome.storage.sync.get(SMART_MUTE_ON_TAB_CHANGE, function (result) {
                if (isTrue(result[SMART_MUTE_ON_TAB_CHANGE])) {
                    unmuteThis(true);
                }
            });
        }
    });
}

chrome.tabs.onActivated.addListener((activeInfo) => {
    handleActiveTabChange();
});

chrome.windows.onFocusChanged.addListener((windowId) => {
    if (windowId === chrome.windows.WINDOW_ID_NONE) {
        return;
    }

    handleActiveTabChange();
});

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    chrome.storage.sync.get(SMART_MUTE_ON_AUDIBLE_CHANGE, function (result) {
        if (isTrue(result[SMART_MUTE_ON_AUDIBLE_CHANGE]) && isTrue(changeInfo.audible)) {
            unmuteThis(true);
        }
    });
});