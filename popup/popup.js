'use strict';


function updateUI() {
}

$('button').each(function () {
    $(this).click(function () {
        let type = $(this).attr('_type');
        let effected = $(this).attr('_effected');

        volumize(type, effected);
    });
});

$('#cb_smartMuteOnAudibleChange').change(function () {
    setStorage(SMART_MUTE_ON_AUDIBLE_CHANGE, $(this).prop('checked'), updateUI);
});

$('#cb_smartMuteOnTabChange').change(function () {
    setStorage(SMART_MUTE_ON_TAB_CHANGE, $(this).prop('checked'), updateUI);
});

$(document).ready(function () {
    chrome.storage.sync.get(SMART_MUTE_ON_TAB_CHANGE, function (result) {
        setBooleanProp('cb_smartMuteOnTabChange', 'checked', result[SMART_MUTE_ON_TAB_CHANGE]);
    });
    chrome.storage.sync.get(SMART_MUTE_ON_AUDIBLE_CHANGE, function (result) {
        setBooleanProp('cb_smartMuteOnAudibleChange', 'checked', result[SMART_MUTE_ON_AUDIBLE_CHANGE]);
    });
});