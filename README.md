# Summary

Mute / Unmute other tabs extension for Google Chrome.

---

# Usage

You can use the popup user interface to mute / unmute other tabs or you can just use the shortcut keys.

| **Command** | **Windows**            | **Mac OS**                |
| :---------: | :--------------------: | :-----------------------: |
| Mute        | `Ctrl + Shift + I`     | `Command + Shift + I`     |
| UnMute      | `Ctrl + Shift + U`     | `Command + Shift + U`     |

## Smart Mute

While the `Smart Mute` feature is active, the audio in the other tabs get muted if the current tab has an audio.

#####  On Play / Pause

When an audible item is started, the other tabs are automatically muted.

#####  On Tab Change

When navigated to another tab and the tab has an audible component, the other tabs are automatically muted.

---

# Feedback

Please feel free to create an issue or contact in case any issue / bug / undesired action happens.

Any other suggestions, recommendations and feature requests are welcomed.

**E-Mail:** <ugurkaya.product.feedback@gmail.com>