'use strict';

const DEBUGGING = true;

const SMART_MUTE_ON_AUDIBLE_CHANGE = "useSmartMuteOnAudibleChange";
const SMART_MUTE_ON_TAB_CHANGE = "useSmartMuteOnTabChange";

function volumize(type, effected) {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (currentTabs) {
        if (isFalse(currentTabs)) {
            return;
        }

        let currentTabId = currentTabs[0].id;

        chrome.tabs.query({
            audible: true,
            muted: (type === 'unmute')
        }, function (tabs) {
            tabs.forEach(function (tab) {
                if (effected === 'all' || (effected === 'this' && tab.id === currentTabId) || (effected === 'others' && tab.id !== currentTabId)) {
                    chrome.tabs.update(tab.id, {
                        muted: (type === 'mute')
                    })
                }
            });
        });
    });
}

function mute(effected) {
    volumize('mute', effected);
}

function unmute(effected) {
    volumize('unmute', effected);
}

function unmuteThis(muteOthers) {
    if (isTrue(muteOthers)) {
        mute('others');
    }
    unmute('this');
}

function setStorage(key, value, callback) {
    chrome.storage.sync.set({[key]: value}, function () {
        if (callback) {
            callback();
        }
    });
}

function setBooleanProp(id, prop, value) {
    $('#' + id).prop(prop, isTrue(value));
}

function isTrue(value) {
    return !isFalse(value);
}

function isFalse(value) {
	if (value === undefined || !value) {
		return false;
	}

	if ((value.constructor === Array && value.length === 0) || String(value).trim().length === 0 || String(value).trim() === 'false') {
		return true;
	}

	for (var attr in value) {
		return false;
	}

	return true;
}

function log(message, ...optionalParams) {
    if (DEBUGGING) {
        if (optionalParams.length > 0) {
            console.debug(message, optionalParams);
        } else {
            console.debug(message);
        }
    }
}
